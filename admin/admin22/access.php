<?php
session_start();

function access($perm){
    if(isset($_SESSION["ACCESS"]) && !$_SESSION["ACCESS"][$perm]){
        header("location:denied.php");
        die;
    }
}
$_SESSION["ACCESS"]["ADD"] =  isset($_SESSION["PERMISSION"]) && trim($_SESSION["PERMISSION"])=="1";
$_SESSION["ACCESS"]["SELL"] = isset($_SESSION["PERMISSION"]) && trim($_SESSION["PERMISSION"])=="2";

var_dump(($_SESSION["PERMISSION"]));
?>
